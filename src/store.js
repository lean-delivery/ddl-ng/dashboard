import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    breadcrumbs: [
      { label: 'Clusters', route: { name: 'clusters' } }
    ],
    sidebar: true,
    config: {}
  },
  mutations: {
    hideSidebar (state) {
      state.sidebar = false
    },

    showSidebar (state) {
      state.sidebar = true
    },

    config (state, config) {
      state.config = config
    },

    breadcrumbs (state, breadcrumbs) {
      state.breadcrumbs = breadcrumbs
    }
  },
  actions: {
    hideSidebar ({ commit }) {
      commit('hideSidebar')
    },

    showSidebar ({ commit }) {
      commit('showSidebar')
    },

    config ({ commit }, config) {
      commit('config', config)
    },

    breadcrumbs ({ commit }, breadcrumbs) {
      commit('breadcrumbs', breadcrumbs)
    }
  }
})
