import Vue from 'vue'
import axios from 'axios'
import ElementUI from 'element-ui'
import VueMoment from 'vue-moment'

import App from './App.vue'
import locale from 'element-ui/lib/locale/lang/en'
import '@/scss/styles.scss'
import router from './router'
import store from './store'
import ApiPlugin from './plugins/api'

import VueClipboards from 'vue-clipboards'

axios.get('/config.json').then(response => {
  window._config = response.data
  store.dispatch('config', response.data)

  router.beforeEach((to, from, next) => {
    let xtoken = localStorage.getItem('_xtoken')

    if (xtoken) {
      return to.name !== 'sign-in' ? next() : next('/')
    } else {
      return to.name === 'sign-in' || to.name === 'sign-in-callback' ? next() : next('/sign-in')
    }
  })

  Vue.use(ElementUI, { locale })
  Vue.use(ApiPlugin)
  Vue.use(VueMoment)
  Vue.use(VueClipboards)

  Vue.config.productionTip = false

  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
})
