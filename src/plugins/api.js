/* eslint-disable no-unused-vars */
import axios from 'axios'

let ApiPlugin = {
  install (Vue, options) {
    const _axios = axios.create({
      baseURL: window._config.API_ENDPOINT,
      timeout: 60 * 1000,
      withCredentials: false,
      headers: {
        'X-Auth-Token': localStorage.getItem('_xtoken')
      }
    })

    _axios.interceptors.response.use(response => {
      return response
    }, error => {
      if (error.response.status === 401 || error.response.status === 403) {
        localStorage.removeItem('_xtoken')
        window.location.reload()
      } else {
        return Promise.reject(error)
      }
    })

    Vue.prototype.$api = {
      getOAuthToken (provider) {
        return axios.get(window._config.OAUTH_ENDPOINT + '/' + provider)
      },

      login (username, password) {
        return axios.post(window._config.API_ENDPOINT + '/login', { username, password })
      },

      getClusters () {
        return _axios.get('/clusters')
      },

      getClusterByID (id) {
        return _axios.get(`/clusters/${id}`)
      },

      getClusterDeploy (cid, did) {
        return _axios.get(`/clusters/${cid}/jobs/${did}`)
      },

      getClusterDeploys (id) {
        return _axios.get(`/clusters/${id}/jobs`)
      },

      getClusterDeployLogs (cid, did) {
        return _axios.get(`/clusters/${cid}/jobs/${did}/logs`)
      },

      getClusterSSHKeyByID (id) {
        return _axios.get(`/clusters/${id}/sshkey`)
      },

      getClusterDescriptionByID(id) {
        return _axios.get(`/clusters/${id}/description`)
      },

      downloadClusterDeployArtifact (cid, did, name) {
        return _axios.get(`/clusters/${cid}/deploys/${did}/artifacts/${name}`)
      },

      generateNewSSHKeyByID (id) {
        return _axios.post(`/clusters/${id}/sshkey/make`)
      },

      createCluster (params) {
        return _axios.post('/clusters', params)
      },

      createClusterNode (id, node) {
        return _axios.post(`/clusters/${id}/nodes`, node)
      },

      createClusterDeploy (id, params) {
        return _axios.post(`/clusters/${id}/jobs`, params)
      },

      updateCluster (id, params) {
        return _axios.put(`/clusters/${id}`, params)
      },

      destroyClusterByID (id, params) {
        return _axios.delete(`/clusters/${id}`, {
        data: params})
      },

      destroyClusterDeploy (cid, did) {
        return _axios.delete(`/clusters/${cid}/jobs/${did}`)
      }
    }
  }
}

export default ApiPlugin
